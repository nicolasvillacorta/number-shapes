package com.example.istriangular_issquare;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    class Number {

        int number;

        public boolean isSquare() {

            double squareRoot = Math.sqrt(number);

            if(squareRoot*squareRoot == number){
                return true;
            } else {
                return false;
            }
        }
        public boolean isTriangular() {

            int x = 1;
            int triangularNumber = 1;

            while(triangularNumber<number) {

                x++;
                triangularNumber = triangularNumber + x;
            }

            if(triangularNumber == number) {
                return true;
            } else {
                return false;
            }

        }

    }

    public void testNumber(View view){
        Log.i("INFO", "Button pressed!");
        String mensaje;

        EditText editText = (EditText) findViewById(R.id.editText);

        if(editText.getText().toString().isEmpty()){
            mensaje = "Por favor ingrese un numero";
        } else{

            Number myNumber = new Number();
            myNumber.number = Integer.parseInt(editText.getText().toString());

            if(myNumber.isSquare() && myNumber.isTriangular()){
                mensaje = "El numero es cuadrado y triangular!";
            } else if(myNumber.isSquare()){
                mensaje = "El numero es cuadrado!";
            } else if(myNumber.isTriangular()){
                mensaje = "El numero es triangular";
            } else {
                mensaje = "El numero no es ni triangular ni cuadrado :(";
            }
        }
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
